@extends('layouts.app')

@section('content')
<div>
@if (session('status'))
    <div class="alert alert-danger">
        {{ session('status') }}
    </div>
@endif                
<form action="{{ route('login') }}" method="post">
    @csrf
    <div class="mb-3">
        <label for="email" class="form-label">Email address</label>
        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="name@example.com" value="{{ old('email') }}">
        @error('email')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">Email address</label>
        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="enter password here">
        @error('password')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="mb-4">
        <div>
            <input type="checkbox" name="remember" id="remember" class="mr-2">
            <label for="remember">Remember me</label>
        </div>
    </div>
    <div class="mb-3">
        <button type="submit" class="btn btn-primary">Login</button>
    </div>
</form>
</div>
@endsection