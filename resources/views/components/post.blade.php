@props(['post' => $post])

<div>
    <h2><a href="{{ route('posts.show', $post) }}">{{$post->title }}</a></h2> <b>by {{$post->user->name}} </b> <br/>
    <p>{{$post->body}}</p>
    <div class="row">
    @can('update', $post)
    <div class="col-1">
    <a href="{{ route('posts.show', $post) }}">
        <button class="btn btn-primary" type="submit">Edit</button>
    </a>
    </div>
    @endcan
    @can('delete', $post)
    <div class="col-1">
    <form action="{{ route('posts.destroy', $post) }}" method="post">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger" type="submit">Delete</button>
    </form>
    </div>
    @endcan
    </div>
</div>