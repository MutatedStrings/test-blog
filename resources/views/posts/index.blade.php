@extends('layouts.app')

@section('content')
    <div>
            @auth
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form action="{{ route('posts.store') }}" method="post">
                    @csrf
                    <div class="mb-3">
                        <label for="title">Title</title>
                        <input type="text" name="title" id="title" class="form-control" placeholder="Title" 
                        value="{{ old('title') }}" />
                    </div>
                    <div class="mb-3">
                        <label for="body">Body</label>
                        <textarea name="body" id="body" cols="30" rows="4" class="form-control" placeholder="Post body" 
                        value="{{ old('body') }}"></textarea>
                    </div>

                    <div>
                        <button type="submit" class="btn btn-primary">Post</button>
                    </div>
                </form>
            @endauth

            @if ($posts->count())
            <div>
                <br />
                @foreach ($posts as $post)
                    <x-post-item :post="$post" /><br />
                @endforeach

                {{ $posts->links() }}
            </div>
            @else
                <p>There are no posts</p>
            @endif
    </div>
@endsection