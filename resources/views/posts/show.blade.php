@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
        <div class="card">
            <div class="card-header">
                {{ $post->title }}
            </div>
            <div class="card-body">
                <b> by {{ $post->user->name }} </b><br /><br />
                @guest
                <p class="mb-2">{{ $post->body }}</p>
                @endguest
                @can('update', $post) 
                <form action="{{ route('posts.update', $post) }}" method="post">
                    @csrf
                    @method('PATCH')
                    <input type="text" value="{{ $post->body }}" name="body" id="body" class="form-control" />
                </form>
                @endcan
            </div>
        </div>
    </div>
@endsection